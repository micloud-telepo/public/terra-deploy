#!/bin/sh -e
# source this file to set the GIT_DESCRIPTOR variable.
#   I.e. "source GIT_DESCRIPTOR.sh"
# If you want the variable visible to sub-processes, you need
# to export it.
#   I.e. "export GIT_DESCRIPTOR"
#
# Script will also print the $GIT_DESCRIPTOR to stdout.

# Shortened Git hash:
GIT_DESCRIPTOR=$(git rev-parse --short=16 HEAD)

# If local commit (HEAD) does not exist in any remote repository, mark "-local":
if test -z "$(git branch -r --contains HEAD)" > /dev/null
then
  GIT_DESCRIPTOR=${GIT_DESCRIPTOR}-local
fi

# If local repository is contaminated with modified/untracked files, mark "-dirty":
if test -n "$(git status -s)" > /dev/null
then
  GIT_DESCRIPTOR=${GIT_DESCRIPTOR}-dirty
fi

echo $GIT_DESCRIPTOR
