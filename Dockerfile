FROM alpine:3.13

# Mirror installed packages from docker image cas-docker-dev/cicd-terraform:0.13.3
RUN apk add --no-cache \
  bash \
  ca-certificates \
  curl \
  expat \
  git \
  libedit \
  ncurses \
  nghttp2 \
  openssh \
  pcre2 \
  readline

# Install tfenv and tgenv, including the latest
# versions of terraform and terragrunt
RUN apk add --no-cache \
  gnupg \
  && git clone https://github.com/tfutils/tfenv.git /opt/tfenv \
  && ln -nfs /opt/tfenv/bin/* /usr/local/bin/ \
  && mkdir ~/.tfenv/ \
  && echo 'trust-tfenv: yes' > ~/.tfenv/use-gpgv \
  && tfenv install latest \
  && tfenv use latest \
  && git clone https://github.com/taosmountain/tgenv.git /opt/tgenv \
  && ln -nfs /opt/tgenv/bin/* /usr/local/bin/ \
  && tgenv install latest \
  && tgenv use latest

ARG GIT_DESCRIPTOR=unset
ENV GIT_DESCRIPTOR=${GIT_DESCRIPTOR}
RUN echo ${GIT_DESCRIPTOR} > /GIT_DESCRIPTOR
